<?php include "superior.php"; ?>

    


    <section id="contact">
       <section id="get-in-touch">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Contato</h2>
                <p class="text-center wow fadeInDown">Deixe sua mensagem, sua sugestão e até seu pedido para fazer seu site. Estaremos sempre disposto a realizar seus projetos.</p>
            </div>
        </div>
    </section><!--/#get-in-touch-->
        <div class="container-wrapper">
            <div class="container">
                <div class="row">
				<div class="col-sm-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.1456944846695!2d-46.65643898502215!3d-23.563210284681976!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59c8da0aa315%3A0xd59f9431f2c9776a!2sAv.+Paulista%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1513611153434" width="100%" height="450" frameborder="0" style="border:0;width:100%;    margin-top: 50px;" allowfullscreen></iframe>
				
				
				</div>
                    <div class="col-sm-6">
                        <div class="contact-form">
                            

                            <address>

                              <p> Telefone: (11) 2030-4050 • 99876-5432</p>
							  <h3>Deixe sua Mensagem:</h3>
                            </address>

                            <form id="main-contact-form" name="contact-form" method="post" action="outlook.php">
                                <div class="form-group" style="width:100%;">
                                    <input type="text" name="nome" class="form-control" placeholder="Nome" required>
                                </div>
                                <div class="form-group" style="width:100%;">
                                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                                <div class="form-group" style="width:100%;">
                                    <input type="text" name="assunto" class="form-control" placeholder="Assunto" required>
                                </div>
                                <div class="form-group" style="width:100%;">
                                    <textarea name="mensagem" class="form-control" rows="8" placeholder="Mensagem" required></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#bottom-->
	
	<?php include "inferior.php"; ?>