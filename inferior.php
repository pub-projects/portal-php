    <footer id="footer">
        <div class="container">
            <div class="row">
			<div class="col-sm-12">
			<ul class="nav navbar-nav nav-rodape">
                        
						<li <?php if($paginaLink == 'automoveis.php') {echo 'class="active"';} ?>><a href="automoveis.php">Automóveis</a></li>
                     
                        <li <?php if($paginaLink == 'revendas.php') {echo 'class="active"';} ?>><a href="revendas.php">Revendas</a></li>
						
						<li <?php if($paginaLink == 'ofertas.php') {echo 'class="active"';} ?>><a href="ofertas.php">Ofertas</a></li>
                        

                        <li <?php if($paginaLink == 'contato.php') {echo 'class="active"';} ?>><a href="contato.php">Contato LINKS</a></li>                  
					
                    </ul>
			
			</div>
                <div class="col-sm-6">
                    &copy; 2017 Hy Carros.
                </div>
                <div class="col-sm-6">
                    <ul class="social-icons">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
               
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
               
                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
          
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                      
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->
	
	<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Busca Detalhada</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="main-contact-form" name="contact-form" method="post" action="outlook.php">
                                <div class="form-group" style="width:100%;">
								
								<select name="tipo" class="form-control" placeholder="Tipo do Veículo">
									<option>Tipo do Veículo</option>
									  <option value="tipo1">Tipo1</option>
									  <option value="tipo2">Tipo2</option>
									  <option value="tipo3">Tipo3</option>
									  <option value="tipo4">Tipo4</option>
								</select>
																	
                                   
                                </div>
                                <div class="form-group" style="width:100%;">
                                    <select name="marca" class="form-control">
										<option>Marca do Veículo</option>
										  <option value="marca1">Marca1</option>
										  <option value="marca2">Marca2</option>
										  <option value="marca3">Marca3</option>
										  <option value="marca4">Marca4</option>
									</select>
                                </div>
                                <div class="form-group" style="width:100%;">
                                    <select name="modelo" class="form-control">
										<option>Modelo do Veículo</option>
										  <option value="modelo1">Modelo1</option>
										  <option value="modelo2">Modelo2</option>
										  <option value="modelo3">Modelo3</option>
										  <option value="modelo4">Modelo4</option>
									</select>
                                </div>
                                <div class="form-group" style="width:100%;    margin-bottom: 0px;">
									<p style="margin:0px;">Ano</p>
								</div>
								<div class="form-group" style="width:50%;">
								<p style="margin:0px;">De: </p>
									<input type="date" class="form-control" name="ano1" placeholder="De">
								</div>
								<div class="form-group" style="width:50%;">
								<p style="margin:0px;">Até: </p>
									<input type="date" class="form-control" name="ano2" placeholder="Até">
                                </div>
								
								
								
								
								 <div class="form-group" style="width:100%;    margin-bottom: 0px;">
									<p style="margin:0px;">Preço</p>
								</div>
								<div class="form-group" style="width:50%;">
								<p style="margin:0px;">De: </p>
									<input type="number" class="form-control" name="preco1" placeholder="0">
								</div>
								<div class="form-group" style="width:50%;">
								<p style="margin:0px;">Até: </p>
									<input type="number" class="form-control" name="preco2" placeholder="100.000,00">
                                </div>
								
								
								 <div class="form-group" style="width:100%;    margin-bottom: 0px;">
									<p style="margin:0px;">Km</p>
								</div>
								<div class="form-group" style="width:50%;">
								<p style="margin:0px;">De: </p>
									<input type="number" class="form-control" name="km1" placeholder="0">
								</div>
								<div class="form-group" style="width:50%;">
								<p style="margin:0px;">Até: </p>
									<input type="number" class="form-control" name="km2" placeholder="100.000,00">
                                </div>
								
                                <button type="submit" class="btn btn-primary">Buscar</button>
                        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>

      </div>
    </div>
  </div>
</div>
	

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mousescroll.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
	    <script src="js/mapa.js"></script>
</body>
</html>