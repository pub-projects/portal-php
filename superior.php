<?php
    $pagina = explode('/',$_SERVER['PHP_SELF']);
    $pag = $pagina[count($pagina) - 1];
	$paginaLink = basename($_SERVER['SCRIPT_NAME']); 
?>
<!DOCTYPE html>

<html lang="pt">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Hy Carros</title>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.png">

</head><!--/head-->

<body id="home" class="homepage">




    <header id="header">
        <nav id="main-menu" class="navbar navbar-default" role="banner">
            <div class="container">
			
		
			 
		
		
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                   
				<a class="navbar-brand" href="index.php"><img src="images/logo/logo.png" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        
						<li <?php if($paginaLink == 'automoveis.php') {echo 'class="active"';} ?>><a href="automoveis.php">Automóveis</a></li>
                     
                        <li <?php if($paginaLink == 'revendas.php') {echo 'class="active"';} ?>><a href="revendas.php">Revendas</a></li>
						
						<li <?php if($paginaLink == 'ofertas.php') {echo 'class="active"';} ?>><a href="ofertas.php">Ofertas</a></li>
                        

                        <li <?php if($paginaLink == 'contato.php') {echo 'class="active"';} ?>><a href="contato.php">Contato LINKS</a></li>                  
					
                    </ul>
					
	  
                </div>
				
				
				
					  
			
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->