
<?php include "superior.php"; ?>
    <section id="about">
        <div class="container">

            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Ofertas</h2>
                <p class="text-center wow fadeInDown">Encontre o seu carro ideal com as melhores, ofertas do mercado.</p>
            </div>

			
			
			
			
			
			

            <div class="row">
                <div class="col-sm-12 wow fadeInLeft">
                    <h3 class="column-title">Veja as Nossas Mehores Ofertas.</h3>
                    
                      
					
                  
                </div>

                <div class="col-sm-12 wow fadeInRight">
                     
                    <div class="media service-box wow fadeInRight">
                        <div class="col-sm-4">
						
								 <img class="img-responsive" src="images/carro1.jpg" style="width:100%" alt="">
							
						</div>
						<div class="col-sm-8">
							
								<h4 class="media-heading">PALIO ED 1.0 MPI 2P e 4P</h4>
								<ul class="detalhes">
									<li><i class="fa fa-calendar" aria-hidden="true"></i> 1998</li>
									<li><i class="fa fa-tachometer" aria-hidden="true"></i> 130.000</li>
									<li><i class="fa fa-paint-brush" aria-hidden="true"></i> Azul</li>
									<li><i class="fa fa-thermometer-full" aria-hidden="true"></i> Gasolina</li>
								</ul>
							
								
								
								<div class="col-sm-12 preco">
						<h4 class="media-heading" style="color:#4c8ad6;float: left;">R$ 10.900,00</h4>
						<a class="btn btn-primary btn-lg" href="#" style="float:right;">Detalhes</a>
						</div>
							
						</div>
						
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="col-sm-4">
						
								 <img class="img-responsive" src="images/carro2.jpg" style="width:100%" alt="">
							
						</div>
						<div class="col-sm-8">
							
								<h4 class="media-heading">COMPASS LIMITED 2.0 4X2 FLEX 16V AUT.</h4>
								<ul class="detalhes">
									<li><i class="fa fa-calendar" aria-hidden="true"></i> 2018</li>
									<li><i class="fa fa-tachometer" aria-hidden="true"></i> 000.000</li>
									<li><i class="fa fa-paint-brush" aria-hidden="true"></i> Branco</li>
									<li><i class="fa fa-thermometer-full" aria-hidden="true"></i> Flex</li>
								</ul>
							
								
								
								<div class="col-sm-12 preco">
						<h4 class="media-heading" style="color:#4c8ad6;float: left;">R$ 139.700,00</h4>
						<a class="btn btn-primary btn-lg" href="#" style="float:right;">Detalhes</a>
						</div>
							
						</div>
						
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="col-sm-4">
						
								 <img class="img-responsive" src="images/carro3.jpg" style="width:100%" alt="">
							
						</div>
						<div class="col-sm-8">
							
								<h4 class="media-heading">FOX 1.0 MI TOTAL FLEX 8V 5P</h4>
								<ul class="detalhes">
									<li><i class="fa fa-calendar" aria-hidden="true"></i> 2013</li>
									<li><i class="fa fa-tachometer" aria-hidden="true"></i> 41.000</li>
									<li><i class="fa fa-paint-brush" aria-hidden="true"></i> Prata</li>
									<li><i class="fa fa-thermometer-full" aria-hidden="true"></i> Flex</li>
								</ul>
							
								
								
								<div class="col-sm-12 preco">
						<h4 class="media-heading" style="color:#4c8ad6;float: left;">R$ 31.900,00</h4>
						<a class="btn btn-primary btn-lg" href="#" style="float:right;">Detalhes</a>
						</div>
							
						</div>
						
                    </div>

              
                   
                </div>
            </div>
        </div>
    </section><!--/#about-->


   

<?php include "inferior.php"; ?>