
<?php include "superior.php"; ?>
    <section id="about">
        <div class="container">

            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Revendas</h2>
                <p class="text-center wow fadeInDown">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
            </div>

            <div class="row">
                <div class="col-sm-12 wow fadeInLeft">
                    <h3 class="column-title">Veja os nosso Automóveis.</h3>
                    
                      
					
                  
                </div>

                <div class="col-sm-12 wow fadeInRight">
                     
                     
                    <div class="media service-box wow fadeInRight">
                        <div class="col-sm-6">
							<div class="pull-left">
								 <img class="img-responsive" src="images/revenda.jpg" style="width:100%" alt="">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="media-body">
								<h4 class="media-heading">Lorem ipsum dolor.</h4>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								<a class="btn btn-primary btn-lg" href="#">Saiba Mais</a>
							</div>
						</div>
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="col-sm-6">
							<div class="pull-left">
								 <img class="img-responsive" src="images/revenda.jpg" style="width:100%" alt="">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="media-body">
								<h4 class="media-heading">Lorem ipsum dolor.</h4>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								<a class="btn btn-primary btn-lg" href="#">Saiba Mais</a>
							</div>
						</div>
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="col-sm-6">
							<div class="pull-left">
								 <img class="img-responsive" src="images/revenda.jpg" style="width:100%" alt="">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="media-body">
								<h4 class="media-heading">Lorem ipsum dolor.</h4>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								<a class="btn btn-primary btn-lg" href="#">Saiba Mais</a>
							</div>
						</div>
                    </div>

             
                   
                </div>
            </div>
        </div>
    </section><!--/#about-->


   

<?php include "inferior.php"; ?>