<?php include "superior.php"; ?>

    <section id="main-slider">
        <div class="owl-carousel">
            <div class="item" style="background-image: url(images/slider/bg2.jpg);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
							<div class="container">
                                <div class="carousel-content">
                                    <h2>Lorem <span>ipsum</span></h2>
                                    <p>Lorem ipsum dolor sit amet.</p>
									<form class="navbar-form navbar-left" style="padding: 20px 0;float:none!important">
										<div class="form-group" style="    width: 100%;">
										  <input type="text" class="form-control" placeholder="Encontre o seu carro aqui">
										</div>
										<button type="submit" class="btn btn-default busca">Buscar</button>
										<a class="btn btn-default busca" href="#" data-toggle="modal" data-target="#exampleModal">Busca Detalhada</a>
									</form>
                                </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<!--
             <div class="item" style="background-image: url(images/slider/bg2.jpg);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2>Lorem<br><span>ipsum</span></h2>
                                    <p>Lorem ipsum dolor sit amet.</p>
                                    <a class="btn btn-primary btn-lg" href="sobre.php">Saiba Mais</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  item-->   

<!--			
			<div class="item" style="background-image: url(images/slider/bg3.jpg);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2>Lorem<br><span>ipsum</span></h2>
                                    <p>Lorem ipsum dolor sit amet.</p>
                                    <a class="btn btn-primary btn-lg" href="sobre.php">Saiba Mais</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> item-->
			
			
        </div><!--/.owl-carousel-->
    </section><!--/#main-slider-->

    <section id="cta" class="wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h2>
                    <p>
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                    </p>
                </div>
                <div class="col-sm-3 text-right">
                    <a class="btn btn-primary btn-lg" href="automoveis.php">Saiba Mais</a>
                </div>
            </div>
        </div>
    </section><!--/#cta-->

	
	
	   

	
	
    <section id="features">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Revendas</h2>
                <p class="text-center wow fadeInDown">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
            </div>
            <div class="row">
               
                <div class="col-sm-12">
                    <div class="media service-box wow fadeInRight">
                        <div class="col-sm-6">
							<div class="pull-left">
								 <img class="img-responsive" src="images/revenda.jpg" style="width:100%" alt="">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="media-body">
								<h4 class="media-heading">Lorem ipsum dolor.</h4>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								<a class="btn btn-primary btn-lg" href="revendas.php">Saiba Mais</a>
							</div>
						</div>
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="col-sm-6">
							<div class="pull-left">
								 <img class="img-responsive" src="images/revenda.jpg" style="width:100%" alt="">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="media-body">
								<h4 class="media-heading">Lorem ipsum dolor.</h4>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								<a class="btn btn-primary btn-lg" href="revendas.php">Saiba Mais</a>
							</div>
						</div>
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="col-sm-6">
							<div class="pull-left">
								 <img class="img-responsive" src="images/revenda.jpg" style="width:100%" alt="">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="media-body">
								<h4 class="media-heading">Lorem ipsum dolor.</h4>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								<a class="btn btn-primary btn-lg" href="revendas.php">Saiba Mais</a>
							</div>
						</div>
                    </div>

                   
					
                </div>
            </div>
        </div>
    </section>



     <section id="blog">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Noticias</h2>
                <p class="text-center wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="blog-post blog-large wow fadeInLeft" data-wow-duration="300ms" data-wow-delay="0ms">
                        <article>
                            <header class="entry-header">
                                <div class="entry-thumbnail">
                                    <img class="img-responsive" src="images/blog/01.jpg" alt="">
                                    <span class="post-format post-format-video"><i class="fa fa-car" aria-hidden="true"></i></span>
                                </div>
                                <div class="entry-date">25 November 2014</div>
                                <h2 class="entry-title"><a href="noticia.php">While now the fated Pequod had been so long afloat this</a></h2>
                            </header>

                            <div class="entry-content">
                                <P>With a blow from the top-maul Ahab knocked off the steel head of the lance, and then handing to the mate the long iron rod remaining, bade him hold it upright, without its touching off the steel head of the lance, and then handing to the mate the long iron rod remaining. without its touching off the steel without its touching off the steel</P>
                                <a class="btn btn-primary" href="noticia.php">Saiba Mais</a>
                            </div>

                           
                        </article>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="blog-post blog-media wow fadeInRight" data-wow-duration="300ms" data-wow-delay="100ms">
                        <article class="media clearfix">
                            <div class="entry-thumbnail pull-left">
                                <img class="img-responsive" src="images/blog/02.jpg" alt="">
                                <span class="post-format post-format-gallery"><i class="fa fa-car" aria-hidden="true"></i></span>
                            </div>
                            <div class="media-body">
                                <header class="entry-header">
                                    <div class="entry-date">01 December 2014</div>
                                    <h2 class="entry-title"><a href="noticia.php">BeReviews was a awesome envent in dhaka</a></h2>
                                </header>

                                <div class="entry-content">
                                    <P>With a blow from the top-maul Ahab knocked off the steel head of the lance, and then handing to the steel</P>
                                    <a class="btn btn-primary" href="noticia.php">Saiba Mais</a>
                                </div>

                             
                            </div>
                        </article>
                    </div>
                    <div class="blog-post blog-media wow fadeInRight" data-wow-duration="300ms" data-wow-delay="200ms">
                        <article class="media clearfix">
                            <div class="entry-thumbnail pull-left">
                                <img class="img-responsive" src="images/blog/03.jpg" alt="">
                                <span class="post-format post-format-audio"><i class="fa fa-cogs" aria-hidden="true"></i></span>
                            </div>
                            <div class="media-body">
                                <header class="entry-header">
                                    <div class="entry-date">03 November 2014</div>
                                    <h2 class="entry-title"><a href="noticia.php">Play list of old bangle  music and gajal</a></h2>
                                </header>

                                <div class="entry-content">
                                    <P>With a blow from the top-maul Ahab knocked off the steel head of the lance, and then handing to the steel</P>
                                    <a class="btn btn-primary" href="noticia.php">Saiba Mais</a>
                                </div>

                              
                            </div>
                        </article>
                    </div>
                </div>
            </div>

        </div>
    </section>




  



<?php include "inferior.php"; ?>