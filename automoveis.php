
<?php include "superior.php"; ?>
    <section id="about">
        <div class="container">

            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Automóveis</h2>
                <p class="text-center wow fadeInDown">Encontre o seu carro ideal com as melhores, ofertas do mercado.</p>
            </div>

            <div class="row">
                <div class="col-sm-12 wow fadeInLeft">
                    <h3 class="column-title">Veja os Nossos Automóveis.</h3>
                    
                      
					
                  
                </div>

                <div class="col-sm-8 wow fadeInLeft">
                     
                    <div class="media service-box wow fadeInRight">
                        <div class="col-sm-4">
						
								 <img class="img-responsive" src="images/carro1.jpg" style="width:100%" alt="">
							
						</div>
						<div class="col-sm-8">
							
								<h4 class="media-heading">PALIO ED 1.0 MPI 2P e 4P</h4>
								<ul class="detalhes">
									<li><i class="fa fa-calendar" aria-hidden="true"></i> 1998</li>
									<li><i class="fa fa-tachometer" aria-hidden="true"></i> 130.000</li>
									<li><i class="fa fa-paint-brush" aria-hidden="true"></i> Azul</li>
									<li><i class="fa fa-thermometer-full" aria-hidden="true"></i> Gasolina</li>
								</ul>
							
								
								
								<div class="col-sm-12 preco">
						<h4 class="media-heading" style="color:#4c8ad6;float: left;">R$ 10.900,00</h4>
						<a class="btn btn-primary btn-lg" href="carro.php" style="float:right;">Detalhes</a>
						</div>
							
						</div>
						
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="col-sm-4">
						
								 <img class="img-responsive" src="images/carro2.jpg" style="width:100%" alt="">
							
						</div>
						<div class="col-sm-8">
							
								<h4 class="media-heading">COMPASS LIMITED 2.0 4X2 FLEX 16V AUT.</h4>
								<ul class="detalhes">
									<li><i class="fa fa-calendar" aria-hidden="true"></i> 2018</li>
									<li><i class="fa fa-tachometer" aria-hidden="true"></i> 000.000</li>
									<li><i class="fa fa-paint-brush" aria-hidden="true"></i> Branco</li>
									<li><i class="fa fa-thermometer-full" aria-hidden="true"></i> Flex</li>
								</ul>
							
								
								
								<div class="col-sm-12 preco">
						<h4 class="media-heading" style="color:#4c8ad6;float: left;">R$ 139.700,00</h4>
						<a class="btn btn-primary btn-lg" href="carro.php" style="float:right;">Detalhes</a>
						</div>
							
						</div>
						
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="col-sm-4">
						
								 <img class="img-responsive" src="images/carro3.jpg" style="width:100%" alt="">
							
						</div>
						<div class="col-sm-8">
							
								<h4 class="media-heading">FOX 1.0 MI TOTAL FLEX 8V 5P</h4>
								<ul class="detalhes">
									<li><i class="fa fa-calendar" aria-hidden="true"></i> 2013</li>
									<li><i class="fa fa-tachometer" aria-hidden="true"></i> 41.000</li>
									<li><i class="fa fa-paint-brush" aria-hidden="true"></i> Prata</li>
									<li><i class="fa fa-thermometer-full" aria-hidden="true"></i> Flex</li>
								</ul>
							
								
								
								<div class="col-sm-12 preco">
						<h4 class="media-heading" style="color:#4c8ad6;float: left;">R$ 31.900,00</h4>
						<a class="btn btn-primary btn-lg" href="carro.php" style="float:right;">Detalhes</a>
						</div>
							
						</div>
						
                    </div>

              
                   
                </div>
				<div class="col-sm-4 wow fadeInRight">
						 <div class="col-sm-12 wow fadeInLeft">
							<h3 class="column-title">Busca Completa</h3>
						</div>
						<form id="main-contact-form" name="contact-form" method="post" action="outlook.php">
                                <div class="form-group" style="width:100%;">
								
								<select name="tipo" class="form-control" placeholder="Tipo do Veículo">
									<option>Tipo do Veículo</option>
									  <option value="tipo1">Tipo1</option>
									  <option value="tipo2">Tipo2</option>
									  <option value="tipo3">Tipo3</option>
									  <option value="tipo4">Tipo4</option>
								</select>
																	
                                   
                                </div>
                                <div class="form-group" style="width:100%;">
                                    <select name="marca" class="form-control">
										<option>Marca do Veículo</option>
										  <option value="marca1">Marca1</option>
										  <option value="marca2">Marca2</option>
										  <option value="marca3">Marca3</option>
										  <option value="marca4">Marca4</option>
									</select>
                                </div>
                                <div class="form-group" style="width:100%;">
                                    <select name="modelo" class="form-control">
										<option>Modelo do Veículo</option>
										  <option value="modelo1">Modelo1</option>
										  <option value="modelo2">Modelo2</option>
										  <option value="modelo3">Modelo3</option>
										  <option value="modelo4">Modelo4</option>
									</select>
                                </div>
                                <div class="form-group" style="width:100%;    margin-bottom: 0px;">
									<p style="margin:0px;">Ano</p>
								</div>
								<div class="form-group" style="width:50%;">
								<p style="margin:0px;">De: </p>
									<input type="date" class="form-control" name="ano1" placeholder="De">
								</div>
								<div class="form-group" style="width:50%;">
								<p style="margin:0px;">Até: </p>
									<input type="date" class="form-control" name="ano2" placeholder="Até">
                                </div>
								
								
								
								
								 <div class="form-group" style="width:100%;    margin-bottom: 0px;">
									<p style="margin:0px;">Preço</p>
								</div>
								<div class="form-group" style="width:50%;">
								<p style="margin:0px;">De: </p>
									<input type="number" class="form-control" name="preco1" placeholder="0">
								</div>
								<div class="form-group" style="width:50%;">
								<p style="margin:0px;">Até: </p>
									<input type="number" class="form-control" name="preco2" placeholder="100.000,00">
                                </div>
								
								
								 <div class="form-group" style="width:100%;    margin-bottom: 0px;">
									<p style="margin:0px;">Km</p>
								</div>
								<div class="form-group" style="width:50%;">
								<p style="margin:0px;">De: </p>
									<input type="number" class="form-control" name="km1" placeholder="0">
								</div>
								<div class="form-group" style="width:50%;">
								<p style="margin:0px;">Até: </p>
									<input type="number" class="form-control" name="km2" placeholder="100.000,00">
                                </div>
								
                                <button type="submit" class="btn btn-primary" style="float:right;">Buscar</button>
                        </form>
				
				</div>
            </div>
        </div>
    </section><!--/#about-->


   

<?php include "inferior.php"; ?>